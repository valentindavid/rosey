Here is an image of Freedesktop-SDK + Linux 4 Tegra tools for Jetson Nano.

JetPack SDK Manager is required to download Cuda for aarch64. The downloaded
files need to be put in directory: `sdkm_downloads`.

You will need QEMU user for x86_64 as linked statically and have it
registered as binfmt_misc to be able to build `image.bst`.

## Build an image:

```
make
```

This will build `checkout/disk.img`, a 16GB image to be flashed to a
SD card.


## Run the ostree server

```
make ostree-serve
```

Note that need to be run on the same machine the image has been built.

## Commit and update to the ostree server

```
make update-ostree
```

The Jetson Nanos using the image should update automatically within 15
minutes. But they still need to be rebooted. Note you can use dbus to
listen to `eos-updater` in your application if you want to
automatically reboot.
