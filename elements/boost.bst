kind: manual

depends:
- freedesktop-sdk.bst:bootstrap-import.bst
- freedesktop-sdk.bst:components/icu.bst
- python36.bst

build-depends:
- freedesktop-sdk.bst:components/which.bst

variables:
  conf: >-
    --prefix="%{install-root}%{prefix}"
    --libdir="%{install-root}%{libdir}"
    --with-toolset=gcc

  install-flags:
    variant=release
    threading=multi
    link=shared
    cflags="${CFLAGS}"
    cxxflags="${CXXFLAGS}"
    linkflags="${LDFLAGS}"
    runtime-link=shared
    --layout=system

config:
  configure-commands:
  - |
    echo "using python : 3.6 : /usr ;" >user-config.jam

  - |
    ./bootstrap.sh %{conf}

  install-commands:
  - |
    ./b2 ${MAKEFLAGS} install %{install-flags} --user-config=user-config.jam

sources:
- kind: git_tag
  url: https://github.com/boostorg/boost.git
  match:
  - 'boost-*.*.*'
  exclude:
  - '*beta*'
  track: master
  ref: boost-1.71.0-0-gafb333b7c5101041f0280b2edf155c55114c9c95
  submodules:
    libs/system:
      url: ../system.git
      checkout: True
    libs/multi_array:
      url: ../multi_array.git
      checkout: True
    libs/math:
      url: ../math.git
      checkout: True
    libs/smart_ptr:
      url: ../smart_ptr.git
      checkout: True
    libs/parameter:
      url: ../parameter.git
      checkout: True
    libs/algorithm:
      url: ../algorithm.git
      checkout: True
    libs/any:
      url: ../any.git
      checkout: True
    libs/concept_check:
      url: ../concept_check.git
      checkout: True
    libs/python:
      url: ../python.git
      checkout: True
    libs/tti:
      url: ../tti.git
      checkout: True
    libs/functional:
      url: ../functional.git
      checkout: True
    libs/config:
      url: ../config.git
      checkout: True
    libs/log:
      url: ../log.git
      checkout: True
    libs/interprocess:
      url: ../interprocess.git
      checkout: True
    libs/exception:
      url: ../exception.git
      checkout: True
    libs/foreach:
      url: ../foreach.git
      checkout: True
    libs/spirit:
      url: ../spirit.git
      checkout: True
    libs/io:
      url: ../io.git
      checkout: True
    libs/disjoint_sets:
      url: ../disjoint_sets.git
      checkout: True
    libs/units:
      url: ../units.git
      checkout: True
    libs/preprocessor:
      url: ../preprocessor.git
      checkout: True
    libs/format:
      url: ../format.git
      checkout: True
    libs/xpressive:
      url: ../xpressive.git
      checkout: True
    libs/integer:
      url: ../integer.git
      checkout: True
    libs/thread:
      url: ../thread.git
      checkout: True
    libs/tokenizer:
      url: ../tokenizer.git
      checkout: True
    libs/timer:
      url: ../timer.git
      checkout: True
    tools/inspect:
      url: ../inspect.git
      checkout: True
    tools/boostbook:
      url: ../boostbook.git
      checkout: True
    libs/regex:
      url: ../regex.git
      checkout: True
    libs/crc:
      url: ../crc.git
      checkout: True
    libs/random:
      url: ../random.git
      checkout: True
    libs/serialization:
      url: ../serialization.git
      checkout: True
    libs/test:
      url: ../test.git
      checkout: True
    libs/date_time:
      url: ../date_time.git
      checkout: True
    libs/logic:
      url: ../logic.git
      checkout: True
    libs/graph:
      url: ../graph.git
      checkout: True
    libs/numeric/conversion:
      url: ../numeric_conversion.git
      checkout: True
    libs/lambda:
      url: ../lambda.git
      checkout: True
    libs/mpl:
      url: ../mpl.git
      checkout: True
    libs/typeof:
      url: ../typeof.git
      checkout: True
    libs/tuple:
      url: ../tuple.git
      checkout: True
    libs/utility:
      url: ../utility.git
      checkout: True
    libs/dynamic_bitset:
      url: ../dynamic_bitset.git
      checkout: True
    libs/assign:
      url: ../assign.git
      checkout: True
    libs/filesystem:
      url: ../filesystem.git
      checkout: True
    libs/function:
      url: ../function.git
      checkout: True
    libs/conversion:
      url: ../conversion.git
      checkout: True
    libs/optional:
      url: ../optional.git
      checkout: True
    libs/property_tree:
      url: ../property_tree.git
      checkout: True
    libs/bimap:
      url: ../bimap.git
      checkout: True
    libs/variant:
      url: ../variant.git
      checkout: True
    libs/array:
      url: ../array.git
      checkout: True
    libs/iostreams:
      url: ../iostreams.git
      checkout: True
    libs/multi_index:
      url: ../multi_index.git
      checkout: True
    tools/bcp:
      url: ../bcp.git
      checkout: True
    libs/ptr_container:
      url: ../ptr_container.git
      checkout: True
    libs/statechart:
      url: ../statechart.git
      checkout: True
    libs/static_assert:
      url: ../static_assert.git
      checkout: True
    libs/range:
      url: ../range.git
      checkout: True
    libs/rational:
      url: ../rational.git
      checkout: True
    libs/iterator:
      url: ../iterator.git
      checkout: True
    tools/build:
      url: ../build.git
      checkout: True
    tools/quickbook:
      url: ../quickbook.git
      checkout: True
    libs/graph_parallel:
      url: ../graph_parallel.git
      checkout: True
    libs/property_map:
      url: ../property_map.git
      checkout: True
    libs/program_options:
      url: ../program_options.git
      checkout: True
    libs/detail:
      url: ../detail.git
      checkout: True
    libs/numeric/interval:
      url: ../interval.git
      checkout: True
    libs/numeric/ublas:
      url: ../ublas.git
      checkout: True
    libs/wave:
      url: ../wave.git
      checkout: True
    libs/type_traits:
      url: ../type_traits.git
      checkout: True
    libs/compatibility:
      url: ../compatibility.git
      checkout: True
    libs/bind:
      url: ../bind.git
      checkout: True
    libs/pool:
      url: ../pool.git
      checkout: True
    libs/proto:
      url: ../proto.git
      checkout: True
    libs/fusion:
      url: ../fusion.git
      checkout: True
    libs/function_types:
      url: ../function_types.git
      checkout: True
    libs/gil:
      url: ../gil.git
      checkout: True
    libs/intrusive:
      url: ../intrusive.git
      checkout: True
    libs/asio:
      url: ../asio.git
      checkout: True
    libs/uuid:
      url: ../uuid.git
      checkout: True
    tools/litre:
      url: ../litre.git
      checkout: True
    libs/circular_buffer:
      url: ../circular_buffer.git
      checkout: True
    libs/mpi:
      url: ../mpi.git
      checkout: True
    libs/unordered:
      url: ../unordered.git
      checkout: True
    libs/signals2:
      url: ../signals2.git
      checkout: True
    libs/accumulators:
      url: ../accumulators.git
      checkout: True
    libs/atomic:
      url: ../atomic.git
      checkout: True
    libs/scope_exit:
      url: ../scope_exit.git
      checkout: True
    libs/flyweight:
      url: ../flyweight.git
      checkout: True
    libs/icl:
      url: ../icl.git
      checkout: True
    libs/predef:
      url: ../predef.git
      checkout: True
    libs/chrono:
      url: ../chrono.git
      checkout: True
    libs/polygon:
      url: ../polygon.git
      checkout: True
    libs/msm:
      url: ../msm.git
      checkout: True
    libs/heap:
      url: ../heap.git
      checkout: True
    libs/coroutine:
      url: ../coroutine.git
      checkout: True
    libs/coroutine2:
      url: ../coroutine2.git
      checkout: True
    libs/ratio:
      url: ../ratio.git
      checkout: True
    libs/numeric/odeint:
      url: ../odeint.git
      checkout: True
    libs/geometry:
      url: ../geometry.git
      checkout: True
    libs/phoenix:
      url: ../phoenix.git
      checkout: True
    libs/move:
      url: ../move.git
      checkout: True
    libs/locale:
      url: ../locale.git
      checkout: True
    tools/auto_index:
      url: ../auto_index.git
      checkout: True
    libs/container:
      url: ../container.git
      checkout: True
    libs/local_function:
      url: ../local_function.git
      checkout: True
    libs/context:
      url: ../context.git
      checkout: True
    libs/type_erasure:
      url: ../type_erasure.git
      checkout: True
    libs/multiprecision:
      url: ../multiprecision.git
      checkout: True
    libs/lockfree:
      url: ../lockfree.git
      checkout: True
    libs/assert:
      url: ../assert.git
      checkout: True
    libs/align:
      url: ../align.git
      checkout: True
    libs/type_index:
      url: ../type_index.git
      checkout: True
    libs/core:
      url: ../core.git
      checkout: True
    libs/throw_exception:
      url: ../throw_exception.git
      checkout: True
    libs/winapi:
      url: ../winapi.git
      checkout: True
    tools/boostdep:
      url: ../boostdep.git
      checkout: True
    libs/lexical_cast:
      url: ../lexical_cast.git
      checkout: True
    libs/sort:
      url: ../sort.git
      checkout: True
    libs/convert:
      url: ../convert.git
      checkout: True
    libs/endian:
      url: ../endian.git
      checkout: True
    libs/vmd:
      url: ../vmd.git
      checkout: True
    libs/dll:
      url: ../dll.git
      checkout: True
    libs/compute:
      url: ../compute.git
      checkout: True
    libs/hana:
      url: ../hana.git
      checkout: True
    libs/metaparse:
      url: ../metaparse.git
      checkout: True
    libs/qvm:
      url: ../qvm.git
      checkout: True
    libs/fiber:
      url: ../fiber.git
      checkout: True
    libs/process:
      url: ../process.git
      checkout: True
    libs/stacktrace:
      url: ../stacktrace.git
      checkout: True
    libs/poly_collection:
      url: ../poly_collection.git
      checkout: True
    libs/beast:
      url: ../beast.git
      checkout: True
    libs/mp11:
      url: ../mp11.git
      checkout: True
    libs/callable_traits:
      url: ../callable_traits.git
      checkout: True
    libs/contract:
      url: ../contract.git
      checkout: True
    tools/check_build:
      url: ../check_build.git
      checkout: True
    libs/container_hash:
      url: ../container_hash.git
      checkout: True
    libs/hof:
      url: ../hof.git
      checkout: True
    libs/yap:
      url: ../yap.git
      checkout: True
    libs/safe_numerics:
      url: ../safe_numerics.git
      checkout: True
    libs/parameter_python:
      url: ../parameter_python.git
      checkout: True
    libs/headers:
      url: ../headers.git
      checkout: True
    tools/boost_install:
      url: ../boost_install.git
      checkout: True
    libs/outcome:
      url: ../outcome.git
      checkout: True
    libs/histogram:
      url: ../histogram.git
      checkout: True
    libs/variant2:
      url: ../variant2.git
      checkout: True
- kind: patch
  path: patches/boost/python3-multiarch.patch
